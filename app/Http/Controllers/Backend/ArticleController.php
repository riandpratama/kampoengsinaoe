<?php

namespace App\Http\Controllers\Backend;

use Alert;
use Storage;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::orderBy('created_at')->get();

        return view('backend.article.index', compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('title_photo')) {
            $filename = $request->file('title_photo')->store('uploads/article/title_photo');
        } else {
            $filename = NULL;
        }

        $article = Article::create([
            'reference' => $request->reference,
            'title' => $request->title,
            'body' => $request->body,
            'title_photo' => $filename,
            'total_view' => 0
        ]);

        Alert::success('BERHASIL DITAMBAHKAN');

        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Article::findOrFail($id);

        return view('backend.article.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Article::findOrFail($id);

        if ($request->file('title_photo') !== null && $request->file('title_photo') !== ''){
            Storage::delete($data->title_photo);
        }
        if ($request->hasFile('title_photo')) {
            $filename = $request->file('title_photo')->store('uploads/article/title_photo');
        } else {
            $filename = $data->title_photo;
        }
        
        $data->slug = null;
        $data->update([
            'title' => $request->title,
            'body' => $request->body,
            'title_photo' => $filename,
        ]);

        Alert::success('BERHASIL DIUBAH');

        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Article::findOrFail($id);

        Storage::delete($data->title_photo);
        $data->delete();

        Alert::success('BERHASIL DIHAPUS');

        return redirect()->route('article.index');
    }
}
