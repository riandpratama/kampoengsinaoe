<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$article = Article::orderBy('created_at', 'DESC')->limit(3)->get();

        return view('frontend.index', compact('article'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('frontend.about');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {
    	$article = Article::orderBy('created_at', 'DESC')->paginate(5);

        $articleTrending = Article::orderBy('total_view', 'desc')->paginate(3);

        return view('frontend.blog', compact('article', 'articleTrending'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function blogShow($slug)
    {
        $article = Article::where('slug', $slug)->first();
        $count = $article->total_view+1;
        $article->update([
            'total_view' => $count
        ]);

        return view('frontend.blog-show', compact('article'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('frontend.contact');
    }
}
