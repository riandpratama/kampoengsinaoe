<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Article extends Model
{
	use Sluggable;
	
    protected $table = 'articles';
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function setReferenceAttribute()
    {
        return $this->attributes['reference'] = '#'.str_random(10);
    }
}
