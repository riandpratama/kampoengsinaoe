<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator Kampoengsianoe',
            'email' => 'kampoengsinaoe15@gmail.com',
            'password' => bcrypt('administrator'),
            'created_at' => Carbon::now()->format('Y-m-d H:m:s'),
        	'updated_at' => Carbon::now()->format('Y-m-d H:m:s')
        ]);
    }
}
