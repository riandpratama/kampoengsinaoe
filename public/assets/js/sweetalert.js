$(document.body).on('click', '.js-submit-confirm', function(event) {
event.preventDefault();
var $form = $(this).closest('form');
swal({
    title: "Apakah anda yakin?",
    text: "Data yang telah di hapus tidak bisa di kembalikan",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: true
  },
  function() {
    $form.submit();
  });
});

$(document.body).on('click', '.js-submit', function(event) {
event.preventDefault();
var $form = $(this).closest('form');
swal({
    title: "Apakah anda yakin ingin menyimpan perubahan ini?",
    text: "",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#124295",
    confirmButtonText: "Iya",
    closeOnConfirm: true
  },
  function() {
    $form.submit();
  });
});

