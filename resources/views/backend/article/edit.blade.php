@extends('adminlte::page')

@section('content')
	<div class="col-md-12">
        <!-- general form elements -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Create Article</h3>
			</div>

			<form role="form" action="{{ route('article.update', $data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="box-body">
					<div class="form-group">
					<label for="title">Title</label>
						<input type="text" class="form-control{{ $errors->has('title') ? ' has-error' : '' }}" id="title" name="title" required="" value="{{ old('title') ? old('title') : $data->title }}">
						@if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
						<label for="image">Image</label>
						<input type="file" name="title_photo" id="image" ><br>
						<img src="{{ asset('storage/'.$data->title_photo) }}" width="300" height="200" alt="">
						@if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
						<label for="content">Content</label>
						<textarea name="body" id="content" cols="30" rows="10" required="">{{ old('body') ? old('body') : $data->body }}</textarea>
						@if ($errors->has('body'))
                            <span class="help-block">
                                <strong>{{ $errors->first('body') }}</strong>
                            </span>
                        @endif
					</div>
				
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
    </div>
@stop

@section('js')
<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
  <script>
    $('textarea').ckeditor({
        "filebrowserImageBrowseUrl" : 'laravel-filemanager?type=Images',
        "filebrowserBrowseUrl" : "public/laravel-filemanager?type=Files"
    });
  </script>
@endsection