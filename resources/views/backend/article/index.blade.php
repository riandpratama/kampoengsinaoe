@extends('adminlte::page')

@section('content')
    
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Article</div>
                
                <div class="panel-body">
                    <a href="{{ route('article.create') }}" class="btn btn-primary" style="margin-bottom: 15px;" ><i class="fa fa-plus"></i>&nbsp;Create Article </a>
					
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Created At</th>
                                    <th width="100px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($article as $item)
                                
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td><img src="{{ asset('storage/'.$item->title_photo) }}" width="150" height="100" alt=""></td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->created_at->diffForHumans() }}</td>
                                    <td>

                                    <p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> 
                                        <a href="{{ route('article.edit', $item->id) }}" class="btn btn-warning btn-xs" type="submit"><span class="glyphicon glyphicon-edit"></span> </a>
                                    </p>
    								
    								<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> <form action="{{ route('article.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        </form></p>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );
</script>
@endsection