@extends('frontend.template.default')

@section('content')

	<!-- Page top section  -->
	<section class="page-top-section set-bg" data-setbg="{{ asset('assets/img/page-top-bg/1.jpg') }}">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2>About us</h2>
					<p>Sukses mendirikan lembaga pembelajaran bahasa inggris. <br><i><b>Muhammad Zamroni </b></i>mulai menerapkan nilai-nilai dalam pendidikan. </p>
				</div>
			</div>
		</div>
	</section>
	<!-- Page top section end  -->


	<!-- About section -->
	<section class="about-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="{{ asset('assets/img/about.png') }}" alt="">
				</div>
				<div class="col-lg-6">
					<div class="about-text">
						<h2>Belajar Tanpa Batas</h2>
						<p><i><b>Zamroni (39)</b></i>, pria kelahiran Buduran Sidoarjo, merupakan pendiri lembaga pembelajaran bahasa inggris "FIC" yang sekarang juga dikenal dengan sebutan <b>"Kampoeng Sinaoe"</b>.</p>
						<p>Suami dari Ida Nurmala itu mulai mendirikan lembaga tersebut sejak 13 tahun yang lalu (2006). <br> Berawal dari membantu salah satu anak tetangga untuk menyelesaikan tugas bahasa inggrisnya, alumnus Universitas Islam Negeri Malang tesebut mampu menjadikan lembaga berbasis komunitas. <br> </p> <p> Terbukti sampai saat ini ada sekitar 250 siswa/i yang tertarik belajar di Kampoeng Sinaoe (KSI) Mendirikan lembaga tidak semudah membalikkan telapak tangan namun berkat kegigihan usaha maupun dorongan keluarga dan masyarakat sekitar, lembaga tersebut mampu berdiri di tengah-tengah masyarakat. <br> Setelah beberapa tahun berjalan lembaga yang berada di kawasan Buduran Sidoarjo itu terus berkembang. Bahkan saat ini para siswa tidak hanya belajara tentang bahasa dari berbagai literatur yang ada melainkan agama, sosial, kemanusiaan hingga sadar akan lingkungan.</p>
						<p>Jadi mereka (siswa) bisa belajar tentang banyak hal Tidak hanya tentang Literatur, tapi penerapan nilai-nilai agama sosial -- Memanusiakan maupun lingkungan," Ujar Zamroni.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- About section end -->

	<!-- Call to action section  -->
	<section class="cta-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 d-flex align-items-center">
					<h2>Belajar Tanpa Batas</h2>
				</div>
				<div class="col-lg-3 text-lg-right" >
					<a href="{{ route('contact') }}" class="site-btn sb-dark">contact us</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Call to action section end  -->

@endsection