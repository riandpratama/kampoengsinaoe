@extends('frontend.template.default')

@section('content')

	<!-- Blog section  -->
	<section class="blog-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 order-1 order-lg-2">
					<div class="blog-post">
						<img src="{{ asset('storage/'.$article->title_photo) }}" alt="{{ $article->title }}">
						<hr>
						<div class="blog-metas">
							<div class="blog-meta meta-cata">created at <a href="">{{ $article->created_at->diffForHumans() }}</a></div>
							<div class="blog-meta meta-author">by <a href="">Administrator</a></div>
						</div>
						<h2>{{ $article->title }} </h2>
						<p>{!! $article->body !!}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end  -->

@endsection