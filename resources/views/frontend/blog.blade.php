@extends('frontend.template.default')

@section('content')

	<!-- Blog section  -->
	<section class="blog-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 sidebar order-2 order-lg-1">
					<div class="sb-widget">
						<form class="sb-search">
							<input type="text" placeholder="Search">
							<button><i class="fa fa-search"></i></button>
						</form>
					</div>
					<div class="sb-widget">
						<h2 class="sb-title">Trending</h2>
						<div class="recent-post">
							@foreach($articleTrending as $item)
							<div class="rp-item">
								<img src="{{ asset('storage/'. $item->title_photo) }}" width="99" height="99" alt="">
								<div class="rp-text">
									<p>{{ $item->title }}</p>
									<div class="rp-date">{{ $item->created_at->diffForHumans() }}</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
					<div class="sb-widget">
						<div class="info-box">
							<h3>Contact Us for Help</h3>
							<p>Quisque orci purus, sodales in est quis, blandit sollicitudin est. Nam ornare ipsum ac accumsan auctor. Donec consequat arcu et commodo interdum. </p>
							<div class="footer-info-box">
								<div class="fib-icon">
									<img src="{{ asset('assets/img/icons/phone.png') }}" alt="" class="">
								</div>
								<div class="fib-text">
									<p>0815 5550 4268<br>kampoengsinaoe15@gmail.com</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 order-1 order-lg-2">
					@foreach ($article as $item)
					<div class="blog-post">
						<div class="blog-thumb set-bg" data-setbg="{{ asset('storage/'.$item->title_photo) }}">
							<div class="blog-date">{{ $item->created_at->diffForHumans() }}</div>
						</div>
						<div class="blog-metas">
							<div class="blog-meta meta-author">by <a href="">Administrator</a></div>
						</div>
						<h2>{{ $item->title }} </h2>
						<p>{!! substr(strip_tags($item->body, '<img src="">'), 0, 200) !!}</p>
						<a href="{{ route('blog.show', $item->slug) }}" class="site-btn read-more">read more</a>
					</div>
					@endforeach

					{{ $article->render() }}
				
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end  -->

	<!-- Call to action section  -->
	<section class="cta-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 d-flex align-items-center">
					<h2>Belajar Tanpa Batas</h2>
				</div>
				<div class="col-lg-3 text-lg-right" >
					<a href="{{ route('contact') }}" class="site-btn sb-dark">contact us</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Call to action section end  -->

@endsection