@extends('frontend.template.default')

@section('content')
	<!-- Page top section  -->
	<section class="page-top-section set-bg" data-setbg="{{ asset('assets/images/foto3.png') }}">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2>Contact</h2>
				</div>
			</div>
		</div>
	</section>
	<!-- Page top section end  -->

	<!-- Map section  -->
	<div class="map-section">
		
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.3035593604386!2d112.7272373147758!3d-7.431621994638415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7e6a6526f1d2d%3A0xd054dd95fa3aa75e!2sKampoeng%20Sinaoe!5e0!3m2!1sid!2sid!4v1575173501724!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>
	<!-- Map section end  -->

	<!-- Contact section   -->
	<section class="contact-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="contact-text">
						<h2>Kenal Lebih Dekat</h2>
						<div class="header-info-box">
							<div class="hib-icon">
								<img src="img/icons/phone.png" alt="" class="">
							</div>
							<div class="hib-text">
								<h6>0815 5550 4268</h6>
								<p>kampoengsinaoe15@gmail.com</p>
							</div>
						</div>
						<div class="header-info-box">
							<div class="hib-icon">
								<img src="img/icons/map-marker.png" alt="" class="">
							</div>
							<div class="hib-text">
								<h6>JL. KH. Khamdani | No.25</h6>
								<p>Siwalanpanji Buduran</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<form class="contact-form">
						<div class="row">
							<div class="col-lg-6">
								<input type="text" placeholder="Your Name">
							</div>
							<div class="col-lg-6">
								<input type="text" placeholder="Your Email">
							</div>
							<div class="col-lg-4">
							</div>
							<div class="col-lg-12">
								<input type="text" placeholder="Subject">
								<textarea class="text-msg" placeholder="Message"></textarea>
								<button class="site-btn" type="submit">send message</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact section end  -->

@endsection