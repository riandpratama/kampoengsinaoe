@extends('frontend.template.default')

@section('content')
	<!-- Hero section  -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hero-item set-bg" data-setbg="{{ asset('assets/images/slide2.jpg')}}">
				<div class="container">
					<div class="row">
						<div class="col-xl-8">
							<h2 style="font-size:4vw;"><span>Kampoeng Sinaoe Pendidikan Multidimensi</span></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="hero-item set-bg" data-setbg="{{ asset('assets/images/slide1.jpg')}}">
				<div class="container">
					<div class="row">
						<div class="col-xl-8">
							<h2 style="font-size:4vw;"><span>Kampoeng Sinaoe Pembelajaran Nuansa Alam</span></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end  -->

	<!-- Services section  -->
	<section class="services-section">
		<div class="services-warp">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/cogwheel.png')}}" alt="">
								</div>
								<h5>Belajar Tanpa Batas</h5>
							</div>
							<p>Sukses mendirikan lembaga pendidikan pembelajaran bahasa Inggris. Muhammad Zamroni mulai menerapkan nilai-nilai dalam pendidikan. </p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/helmet.png')}}" alt="">
								</div>
								<h5>Nuansa Alam Kampoeng Sinaoe</h5>
							</div>
							<p>Konsep kelas yang dibangun berbentuk layaknya gazebo yang berpondasi bambu dan beratapkan gazebo, Siswa belajar dengan cara lesehan.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/wind-engine.png') }}" alt="">
								</div>
								<h5>Pendidikan Multidimensi</h5>
							</div>
							<p>Kampoeng Sinaoe, lembaga pembelajaran berbasis komunitas yang mampu menerapkan metode pembelajaran yang sesuai konsep John Dewey. </p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/pollution.png') }}" alt="">
								</div>
								<h5>Jujukan Orang Asing</h5>
							</div>
							<p><i>"Education is not preparation for life, educations is life itslife"</i> pendidikan bukan persiapan untuk hidup, pendidikan adalah hidup itu sendiri. </p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/pumpjack.png') }}" alt="">
								</div>
								<h5>Semarak Ramadhan</h5>
							</div>
							<p>Selain lingkungan yang nyaman, kondisi KampoengSinaoe juga mampu menambah tingkat religius kita. Salah satunya suasana ala santri. </p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<img src="{{ asset('assets/img/icons/light-bulb.png') }}" alt="">
								</div>
								<h5>Kampoeng Sinaoe</h5>
							</div>
							<p>Setelah shalat, pemebelajaran bisa dilanjutkan kembali. Aturan ini bagian dari penerapan nilai-nilai keagamaan yang ada di Kampoeng Sinaoe. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Services section end  -->

	<!-- Features section   -->
	<section class="features-section spad set-bg" data-setbg="{{ asset('assets/img/features-bg.jpg') }}">
		<div class="container">
			<div class="row">

				@foreach($article as $item)
				<div class="col-lg-4 col-md-6">
					<div class="feature-box">
						<img src="{{ asset('storage/'.$item->title_photo) }}" alt="">
						<div class="fb-text">
							<h5>{{ $item->title }}</h5>
							<a href="" class="fb-more-btn">Read More</a>
						</div>
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</section>
	<!-- Features section end  -->

	<!-- Video section  -->
	<section class="video-section spad" >
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="video-text">
						<h2>Tempat Favorit Orang Asing di Sidoarjo</h2>
						<p>Apakah kamu ingin berbicara bahasa Inggris dengan lancar?. Ingin belajar bahasa inggris dengan native speaker secara langsung? kalau mau, datanglah ke KampoengSinaoe Sidoarjo. Disana, kamu bisa mengobrol dan berdiskusi dengan native kapanpun kamu mau. Kamu juga bisa menanyakan apapun, mulai dari budaya, politik, hingga tentang pendidikan.</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="video-box set-bg" data-setbg="{{ asset('assets/images/logo-ks.jpg') }}">
						<a href="https://www.youtube.com/watch?v=vWhGwBrLdjk" class="video-popup">
							<i class="fa fa-play"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Video section end  -->


	<!-- Testimonial section -->
	<section class="testimonial-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<div class="testimonial-bg set-bg" data-setbg="{{ asset('assets/images/foto1.jpg') }}"></div>
				</div>
				<div class="col-lg-6 p-0">
					<div class="testimonial-box">
						<div class="testi-box-warp">
							<h2>Student’s Testimonials</h2>
							<div class="testimonial-slider owl-carousel">
								<div class="testimonial">
									<p>I can improve my english skill so that i’m more confident to speak english. in GEC i am not only study but also have new family and togetherness. recomended bangetttt pokona mahhh</p>
									<img src="{{ asset('assets/img/testi1.png') }}" alt="" class="testi-thumb">
									<br>
								</div>
								<div class="testimonial">
									<p>I can be confidence to speak English and I get a lot of experience and friends. I also get many lessons in the as like Speaking, grammar, vocabulary, pronunciation and many other….</p>
									<img src="{{ asset('assets/img/testi2.png') }}" alt="" class="testi-thumb">
								</div>
								<div class="testimonial">
									<p>KampoengSinaoe Keren Banget, Seru Banget Pengajar Sabar Banget, Kekeluargaan Kerasa Banget, Nggak Boring Apalagi Garing, Recomended Banget Buat Kalian Yang Mau Belajar Bahasa Inggris.</p>
									<img src="{{ asset('assets/img/testi3.png') }}" alt="" class="testi-thumb">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial section end  -->


	<!-- Call to action section  -->
	<section class="cta-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 d-flex align-items-center">
					<h2>Belajar Tanpa Batas</h2>
				</div>
				<div class="col-lg-3 text-lg-right" >
					<a href="#" class="site-btn sb-dark">contact us</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Call to action section end  -->

	
@endsection
