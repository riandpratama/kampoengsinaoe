<!DOCTYPE html>
<html lang="en">

@include('frontend.template.partials._head')

<body>
	<!-- Preloader -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
    <!-- /Preloader -->
	
	@include('frontend.template.partials._header')
		
    @yield('content')

    @include('frontend.template.partials._footer')

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->

    @include('frontend.template.partials._javascript')
</body>

</html>