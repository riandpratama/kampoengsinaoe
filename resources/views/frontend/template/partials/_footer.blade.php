<!-- Footer section -->
<footer class="footer-section spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6">
				<div class="footer-widget about-widget">
					<img src="{{ asset('assets/images/video.png') }}" width="210" alt="">
					<p>Lembaga Pendidikan ramah lingkungan mempunyai peran penting terhadap hasil pembelajaran karena selain mengajar guru juga bisa menanamkan kepedulian akan pentingnya lingkungan </p>
					<div class="footer-social">
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-instagram"></i></a>
						<a href=""><i class="fa fa-envelope"></i></a>
						<a href=""><i class="fa fa-yahoo"></i></a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="footer-widget">
					<h2 class="fw-title">Useful Resources</h2>
					<p><i>"Selain belajar materi pelajaran siswa juga bisa mengetahui bahwa merawat lingkungan itu penting"</i></p>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="footer-widget">
					<h2 class="fw-title">Our Solutions</h2>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About Us</a></li>
						<li><a href="">Blog</a></li>
						<li><a href="">Chemical Research</a></li>
						<li><a href="">Contact</a></li>
						<li><a href="">Gallery</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-7">
				<div class="footer-widget">
					<h2 class="fw-title">Contact Us</h2>
					<div class="footer-info-box">
						<div class="fib-icon">
							<img src="{{ asset('assets/img/icons/map-marker.png ') }}" alt="" class="">
						</div>
						<div class="fib-text">
							<p>JL. KH. Khamdani | No.25,<br>Siwalanpanji Buduran Sidoarjo</p>
						</div>
					</div>
					<div class="footer-info-box">
						<div class="fib-icon">
							<img src="{{ asset('assets/img/icons/phone.png ') }}" alt="" class="">
						</div>
						<div class="fib-text">
							<p>0815 5550 4268<br>kampoengsinaoe15@gmail.com</p>
						</div>
					</div>
					<form class="footer-search">
						<input type="text" placeholder="Search">
						<button><i class="fa fa-search"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-buttom">
		<div class="container">
		<div class="row">
			<div class="col-lg-4 order-2 order-lg-1 p-0">
				<div class="copyright"> 
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Kampoengsinaoe
				</div>
			</div>
			<div class="col-lg-7 order-1 order-lg-2 p-0">
				<ul class="footer-menu">
					<li class="{{ Request::segment(1) === '/' ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>
					<li class="{{ Request::segment(1) === 'about' ? 'active' : null }}"><a href="{{ route('about') }}">About us</a></li>
					<li class="{{ Request::segment(1) === 'blog' ? 'active' : null }}"><a href="{{ route('blog') }}">Blog</a></li>
					<li class="{{ Request::segment(1) === 'contact' ? 'active' : null }}"><a href="{{ route('contact') }}">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
	</div>
</footer>
<!-- Footer section end -->