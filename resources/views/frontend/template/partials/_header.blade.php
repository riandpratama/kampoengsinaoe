<!-- Header section  -->
<header class="header-section clearfix">
	<div class="header-top">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<p>Kampoeng Sinaoe</p>
				</div>
				<div class="col-md-6 text-md-right">
					<p>Belajar Tanpa Batas</p>
				</div>
			</div>
		</div>
	</div>
	<div class="site-navbar">
		<!-- Logo -->
		<a href="/" class="site-logo">
			<img src="{{ asset('assets/images/logo-ks-3.png') }}" width="210" alt="">
		</a>
		<div class="header-right">
			<div class="header-info-box">
				<div class="hib-icon">
					<i class="fa fa-phone"></i><br>
					<i class="fa fa-instagram"></i>
				</div>
				<div class="hib-text">
					<h6>0815 5550 4268 </h6>
					<h6 style="padding-top: 13px;">kampoeng_sinaoe</h6>
				</div>
			</div>
			<div class="header-info-box">
				<div class="hib-icon">
					<i class="fa fa-map-marker"></i>
				</div>
				<div class="hib-text">
					<h6>JL. KH. Khamdani | No.25</h6>
					<h6 style="padding-top: 13px;">Siwalanpanji Buduran</h6>
				</div>
			</div>
			<button class="search-switch"><i class="fa fa-search"></i></button>
		</div>
		<!-- Menu -->
		<nav class="site-nav-menu">
			<ul>
				<li class="{{ Request::segment(1) === '/' ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>
				<li class="{{ Request::segment(1) === 'about' ? 'active' : null }}"><a href="{{ route('about') }}">About us</a></li>
				<li class="{{ Request::segment(1) === 'blog' ? 'active' : null }}"><a href="{{ route('blog') }}">Blog</a></li>
				<li class="{{ Request::segment(1) === 'contact' ? 'active' : null }}"><a href="{{ route('contact') }}">Contact</a></li>
			</ul>
		</nav>

	</div>
</header>
<!-- Header section end 