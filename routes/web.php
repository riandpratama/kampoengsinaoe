<?php

Route::get('/cache', function(){
	Artisan::call('config:cache');
});

Route::group(['namespace' => 'Frontend'], function(){
	Route::get('/', 'MainController@index');
	Route::get('/blog', 'MainController@blog')->name('blog');
	Route::get('/about', 'MainController@about')->name('about');
	Route::get('/contact', 'MainController@contact')->name('contact');

	Route::get('/blog/{slug}', 'MainController@blogShow')->name('blog.show');
});

// Authentication
Route::get('/ks/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function(){

	Route::get('article', 'ArticleController@index')->name('article.index');
	Route::get('article/create', 'ArticleController@create')->name('article.create');
	Route::post('article/store', 'ArticleController@store')->name('article.store');
	Route::get('article/{id}/edit', 'ArticleController@edit')->name('article.edit');
	Route::post('article/{id}/update', 'ArticleController@update')->name('article.update');
	Route::get('article/{id}/destroy', 'ArticleController@destroy')->name('article.destroy');
	
});

